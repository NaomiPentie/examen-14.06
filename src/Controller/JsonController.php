<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class JsonController extends AbstractController
{
    /**
     * @Route("/json", name="json")
     */
    public function index()
    {
        $students = $this->getDoctrine()
        ->getRepository(Student::class)
        ->findAll();
        $serializer = $this->get('serializer');
        $response = $serializer->serialize($students,'json');
        return new Response($response);
    }
}
